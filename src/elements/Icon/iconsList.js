/** stroke  **/

import alarm from '../../assets/svgIcons/stroke/alarm.svg'
import atoms from '../../assets/svgIcons/stroke/atoms.svg'
import basket from '../../assets/svgIcons/stroke/basket.svg'
import bin from '../../assets/svgIcons/stroke/bin.svg'
import bioTech from '../../assets/svgIcons/stroke/bioTech.svg'
import bitCoin from '../../assets/svgIcons/stroke/bitCoin.svg'
import bottle from '../../assets/svgIcons/stroke/bottle.svg'
import bulletList from '../../assets/svgIcons/stroke/bulletList.svg'
import camera from '../../assets/svgIcons/stroke/camera.svg'
import chatboxRound from '../../assets/svgIcons/stroke/chatboxRound.svg'
import chatboxSquare from '../../assets/svgIcons/stroke/chatboxSquare.svg'
import chats from '../../assets/svgIcons/stroke/chats.svg'
import clip from '../../assets/svgIcons/stroke/clip.svg'
import cloudDownload from '../../assets/svgIcons/stroke/cloudDownload.svg'
import cloudUpload from '../../assets/svgIcons/stroke/cloudUpload.svg'
import costCenter from '../../assets/svgIcons/stroke/costCenter.svg'
import coupon from '../../assets/svgIcons/stroke/coupon.svg'
import dashboard from '../../assets/svgIcons/stroke/dashboard.svg'
import document from '../../assets/svgIcons/stroke/document.svg'
import dollar from '../../assets/svgIcons/stroke/dollar.svg'
import dollarCoins from '../../assets/svgIcons/stroke/dollarCoins.svg'
import dollarSign from '../../assets/svgIcons/stroke/dollarSign.svg'
import education from '../../assets/svgIcons/stroke/education.svg'
import energy from '../../assets/svgIcons/stroke/energy.svg'
import euro from '../../assets/svgIcons/stroke/euro.svg'
import filter from '../../assets/svgIcons/stroke/filter.svg'
import finance from '../../assets/svgIcons/stroke/finance.svg'
import gridView from '../../assets/svgIcons/stroke/gridView.svg'
import growth from '../../assets/svgIcons/stroke/growth.svg'
import headphone from '../../assets/svgIcons/stroke/headphone.svg'
import heart from '../../assets/svgIcons/stroke/heart.svg'
import historicalData from '../../assets/svgIcons/stroke/historicalData.svg'
import home from '../../assets/svgIcons/stroke/home.svg'
import honeycomb from '../../assets/svgIcons/stroke/honeycomb.svg'
import income from '../../assets/svgIcons/stroke/income.svg'
import keys from '../../assets/svgIcons/stroke/keys.svg'
import language from '../../assets/svgIcons/stroke/language.svg'
import laptopAndMobile from '../../assets/svgIcons/stroke/laptopAndMobile.svg'
import layer from '../../assets/svgIcons/stroke/layer.svg'
import listView from '../../assets/svgIcons/stroke/listView.svg'
import location from '../../assets/svgIcons/stroke/location.svg'
import lock from '../../assets/svgIcons/stroke/lock.svg'
import logistics from '../../assets/svgIcons/stroke/logistics.svg'
import logisticUp from '../../assets/svgIcons/stroke/logisticUp.svg'
import message from '../../assets/svgIcons/stroke/message.svg'
import mic from '../../assets/svgIcons/stroke/mic.svg'
import microscope from '../../assets/svgIcons/stroke/microscope.svg'
import money from '../../assets/svgIcons/stroke/money.svg'
import movie from '../../assets/svgIcons/stroke/movie.svg'
import pencil from '../../assets/svgIcons/stroke/pencil.svg'
import people from '../../assets/svgIcons/stroke/people.svg'
import people2 from '../../assets/svgIcons/stroke/people2.svg'
import person from '../../assets/svgIcons/stroke/person.svg'
import phone from '../../assets/svgIcons/stroke/phone.svg'
import pipette from '../../assets/svgIcons/stroke/pipette.svg'
import plane from '../../assets/svgIcons/stroke/plane.svg'
import posting from '../../assets/svgIcons/stroke/posting.svg'
import pound from '../../assets/svgIcons/stroke/pound.svg'
import puzzle from '../../assets/svgIcons/stroke/puzzle.svg'
import reaction from '../../assets/svgIcons/stroke/reaction.svg'
import repost from '../../assets/svgIcons/stroke/repost.svg'
import roundFlask from '../../assets/svgIcons/stroke/roundFlask.svg'
import settings from '../../assets/svgIcons/stroke/settings.svg'
import share from '../../assets/svgIcons/stroke/share.svg'
import speaker from '../../assets/svgIcons/stroke/speaker.svg'
import testTube from '../../assets/svgIcons/stroke/testTube.svg'
import tools from '../../assets/svgIcons/stroke/tools.svg'
import triangularFlask from '../../assets/svgIcons/stroke/triangularFlask.svg'
import truck from '../../assets/svgIcons/stroke/truck.svg'
import watch from '../../assets/svgIcons/stroke/watch.svg'

/** filled **/

import filledAlarm from '../../assets/svgIcons/filled/alarm.svg'
import filledAtoms from '../../assets/svgIcons/filled/atoms.svg'
import filledBasket from '../../assets/svgIcons/filled/basket.svg'
import filledBin from '../../assets/svgIcons/filled/bin.svg'
import filledBioTech from '../../assets/svgIcons/filled/bioTech.svg'
import filledBitCoin from '../../assets/svgIcons/filled/bitCoin.svg'
import filledBottle from '../../assets/svgIcons/filled/bottle.svg'
import filledBulletList from '../../assets/svgIcons/filled/bulletList.svg'
import filledCamera from '../../assets/svgIcons/filled/camera.svg'
import filledChatboxRound from '../../assets/svgIcons/filled/chatboxRound.svg'
import filledChatboxSquare from '../../assets/svgIcons/filled/chatboxSquare.svg'
import filledChats from '../../assets/svgIcons/filled/chats.svg'
import filledClip from '../../assets/svgIcons/filled/clip.svg'
import filledCloudDownload from '../../assets/svgIcons/filled/cloudDownload.svg'
import filledCloudUpload from '../../assets/svgIcons/filled/cloudUpload.svg'
import filledCostCenter from '../../assets/svgIcons/filled/costCenter.svg'
import filledCoupon from '../../assets/svgIcons/filled/coupon.svg'
import filledDashboard from '../../assets/svgIcons/filled/dashboard.svg'
import filledDocument from '../../assets/svgIcons/filled/document.svg'
import filledDollar from '../../assets/svgIcons/filled/dollar.svg'
import filledDollarCoins from '../../assets/svgIcons/filled/dollarCoins.svg'
import filledDollarSign from '../../assets/svgIcons/filled/dollarSign.svg'
import filledEducation from '../../assets/svgIcons/filled/education.svg'
import filledEnergy from '../../assets/svgIcons/filled/energy.svg'
import filledEuro from '../../assets/svgIcons/filled/euro.svg'
import filledFilter from '../../assets/svgIcons/filled/filter.svg'
import filledFinance from '../../assets/svgIcons/filled/finance.svg'
import filledGridView from '../../assets/svgIcons/filled/gridView.svg'
import filledGrowth from '../../assets/svgIcons/filled/growth.svg'
import filledHeadphone from '../../assets/svgIcons/filled/headphone.svg'
import filledHeart from '../../assets/svgIcons/filled/heart.svg'
import filledHistoricalData from '../../assets/svgIcons/filled/historicalData.svg'
import filledHome from '../../assets/svgIcons/filled/home.svg'
import filledHoneycomb from '../../assets/svgIcons/filled/honeycomb.svg'
import filledIncome from '../../assets/svgIcons/filled/income.svg'
import filledKeys from '../../assets/svgIcons/filled/keys.svg'
import filledLanguage from '../../assets/svgIcons/filled/language.svg'
import filledLaptopAndMobile from '../../assets/svgIcons/filled/laptopAndMobile.svg'
import filledLayer from '../../assets/svgIcons/filled/layer.svg'
import filledListView from '../../assets/svgIcons/filled/listView.svg'
import filledLocation from '../../assets/svgIcons/filled/location.svg'
import filledLock from '../../assets/svgIcons/filled/lock.svg'
import filledLogistics from '../../assets/svgIcons/filled/logistics.svg'
import filledLogisticUp from '../../assets/svgIcons/filled/logisticUp.svg'
import filledMessage from '../../assets/svgIcons/filled/message.svg'
import filledMic from '../../assets/svgIcons/filled/mic.svg'
import filledMicroscope from '../../assets/svgIcons/filled/microscope.svg'
import filledMoney from '../../assets/svgIcons/filled/money.svg'
import filledMovie from '../../assets/svgIcons/filled/movie.svg'
import filledPencil from '../../assets/svgIcons/filled/pencil.svg'
import filledPeople from '../../assets/svgIcons/filled/people.svg'
import filledPeople2 from '../../assets/svgIcons/filled/people2.svg'
import filledPerson from '../../assets/svgIcons/filled/person.svg'
import filledPhone from '../../assets/svgIcons/filled/phone.svg'
import filledPipette from '../../assets/svgIcons/filled/pipette.svg'
import filledPlane from '../../assets/svgIcons/filled/plane.svg'
import filledPosting from '../../assets/svgIcons/filled/posting.svg'
import filledPound from '../../assets/svgIcons/filled/pound.svg'
import filledPuzzle from '../../assets/svgIcons/filled/puzzle.svg'
import filledReaction from '../../assets/svgIcons/filled/reaction.svg'
import filledRepost from '../../assets/svgIcons/filled/repost.svg'
import filledRoundFlask from '../../assets/svgIcons/filled/roundFlask.svg'
import filledSettings from '../../assets/svgIcons/filled/settings.svg'
import filledShare from '../../assets/svgIcons/filled/share.svg'
import filledSpeaker from '../../assets/svgIcons/filled/speaker.svg'
import filledTestTube from '../../assets/svgIcons/filled/testTube.svg'
import filledTools from '../../assets/svgIcons/filled/tools.svg'
import filledTriangularFlask from '../../assets/svgIcons/filled/triangularFlask.svg'
import filledTruck from '../../assets/svgIcons/filled/truck.svg'
import filledWatch from '../../assets/svgIcons/filled/watch.svg'

/** glyphs **/

import add from '../../assets/svgIcons/glyphs/add.svg'
import arrowCheck from '../../assets/svgIcons/glyphs/arrowCheck.svg'
import arrowChevron from '../../assets/svgIcons/glyphs/arrowChevron.svg'
import arrowDoubleLeft from '../../assets/svgIcons/glyphs/arrowDoubleLeft.svg'
import arrowDoubleRight from '../../assets/svgIcons/glyphs/arrowDoubleRight.svg'
import arrowDown from '../../assets/svgIcons/glyphs/arrowDown.svg'
import arrowLeft from '../../assets/svgIcons/glyphs/arrowLeft.svg'
import arrowRight from '../../assets/svgIcons/glyphs/arrowRight.svg'
import arrowTop from '../../assets/svgIcons/glyphs/arrowTop.svg'
import attention from '../../assets/svgIcons/glyphs/attention.svg'
import bell from '../../assets/svgIcons/glyphs/bell.svg'
import calendar from '../../assets/svgIcons/glyphs/calendar.svg'
import checkboxEmpty from '../../assets/svgIcons/glyphs/checkboxEmpty.svg'
import checkboxFilled from '../../assets/svgIcons/glyphs/checkboxFilled.svg'
import close from '../../assets/svgIcons/glyphs/close.svg'
import dot from '../../assets/svgIcons/glyphs/dot.svg'
import exportFile from '../../assets/svgIcons/glyphs/exportFile.svg'
import favorite from '../../assets/svgIcons/glyphs/favorite.svg'
import filterGlyph from '../../assets/svgIcons/glyphs/filter.svg'
import mediaAudio from '../../assets/svgIcons/glyphs/mediaAudio.svg'
import mediaFastForward from '../../assets/svgIcons/glyphs/mediaFastForward.svg'
import mediaPause from '../../assets/svgIcons/glyphs/mediaPause.svg'
import mediaPlay from '../../assets/svgIcons/glyphs/mediaPlay.svg'
import mediaRewind from '../../assets/svgIcons/glyphs/mediaRewind.svg'
import mediaStop from '../../assets/svgIcons/glyphs/mediaStop.svg'
import minus from '../../assets/svgIcons/glyphs/minus.svg'
import options from '../../assets/svgIcons/glyphs/options.svg'
import placeholder from '../../assets/svgIcons/glyphs/placeholder.svg'
import plus from '../../assets/svgIcons/glyphs/plus.svg'
import progressBarCheckMark from '../../assets/svgIcons/glyphs/progressBarCheckMark.svg'
import progressBarComingUp from '../../assets/svgIcons/glyphs/progressBarComingUp.svg'
import progressBarDisabled from '../../assets/svgIcons/glyphs/progressBarDisabled.svg'
import progressBarIndicator from '../../assets/svgIcons/glyphs/progressBarIndicator.svg'
import radioButtonFilled from '../../assets/svgIcons/glyphs/radioButtonFilled.svg'
import radioButtonEmpty from '../../assets/svgIcons/glyphs/radioButtonEmpty.svg'
import radioButtonActive from '../../assets/svgIcons/glyphs/radioButtonActive.svg'
import radioButtonHover from '../../assets/svgIcons/glyphs/radioButtonHover.svg'
import search from '../../assets/svgIcons/glyphs/search.svg'
import settingsGlyph from '../../assets/svgIcons/glyphs/settings.svg'
import star from '../../assets/svgIcons/glyphs/star.svg'
import tooltipFilled from '../../assets/svgIcons/glyphs/tooltipFilled.svg'
import tooltipEmpty from '../../assets/svgIcons/glyphs/tooltipEmpty.svg'
import upload from '../../assets/svgIcons/glyphs/upload.svg'

/** for component purposes **/

import circleX from '../../assets/svgIcons/glyphs/circleX.svg'
import dotHalf from '../../assets/svgIcons/glyphs/dotHalf.svg'
import dots from '../../assets/svgIcons/glyphs/dots.svg'
import goods from '../../assets/svgIcons/glyphs/goods.svg'
import info from '../../assets/svgIcons/glyphs/info.svg'
import infoFilled from '../../assets/svgIcons/glyphs/infoFilled.svg'
import infoM from '../../assets/svgIcons/glyphs/infoM.svg'
import infoCircle from '../../assets/svgIcons/glyphs/infoCircle.svg'
import logo from '../../assets/svgIcons/glyphs/logo.svg'
import radioButton from '../../assets/svgIcons/glyphs/radioButton.svg'
import rental from '../../assets/svgIcons/glyphs/rental.svg'
import service from '../../assets/svgIcons/glyphs/service.svg'
import starHalf from '../../assets/svgIcons/glyphs/starHalf.svg'
import warning from '../../assets/svgIcons/glyphs/warning.svg'
import facebook from '../../assets/svgIcons/glyphs/social/facebook.svg'
import slack from '../../assets/svgIcons/glyphs/social/slack.svg'
import instagram from '../../assets/svgIcons/glyphs/social/instagram.svg'
import mail from '../../assets/svgIcons/glyphs/social/mail.svg'
import snapchat from '../../assets/svgIcons/glyphs/social/snapchat.svg'
import salesforce from '../../assets/svgIcons/glyphs/social/salesforce.svg'
import twitter from '../../assets/svgIcons/glyphs/social/twitter.svg'
import skype from '../../assets/svgIcons/glyphs/social/skype.svg'
import linkedin from '../../assets/svgIcons/glyphs/social/linkedin.svg'
import teams from '../../assets/svgIcons/glyphs/social/teams.svg'
import flickr from '../../assets/svgIcons/glyphs/social/flickr.svg'
import xing from '../../assets/svgIcons/glyphs/social/xing.svg'
import warningM from '../../assets/svgIcons/glyphs/warningM.svg'

export default {
  stroke: {
    alarm,
    atoms,
    basket,
    bin,
    bioTech,
    bitCoin,
    bottle,
    bulletList,
    camera,
    chatboxRound,
    chatboxSquare,
    chats,
    clip,
    cloudDownload,
    cloudUpload,
    costCenter,
    coupon,
    dashboard,
    document,
    dollar,
    dollarCoins,
    dollarSign,
    education,
    energy,
    euro,
    filter,
    finance,
    gridView,
    growth,
    headphone,
    heart,
    historicalData,
    home,
    honeycomb,
    income,
    keys,
    language,
    laptopAndMobile,
    layer,
    listView,
    location,
    lock,
    logistics,
    logisticUp,
    message,
    mic,
    microscope,
    money,
    movie,
    pencil,
    people,
    people2,
    person,
    phone,
    pipette,
    plane,
    posting,
    pound,
    puzzle,
    reaction,
    repost,
    roundFlask,
    settings,
    share,
    speaker,
    testTube,
    tools,
    triangularFlask,
    truck,
    watch,
  },

  filled: {
    alarm: filledAlarm,
    atoms: filledAtoms,
    basket: filledBasket,
    bin: filledBin,
    bioTech: filledBioTech,
    bitCoin: filledBitCoin,
    bottle: filledBottle,
    bulletList: filledBulletList,
    camera: filledCamera,
    chatboxRound: filledChatboxRound,
    chatboxSquare: filledChatboxSquare,
    chats: filledChats,
    clip: filledClip,
    cloudDownload: filledCloudDownload,
    cloudUpload: filledCloudUpload,
    costCenter: filledCostCenter,
    coupon: filledCoupon,
    dashboard: filledDashboard,
    document: filledDocument,
    dollar: filledDollar,
    dollarCoins: filledDollarCoins,
    dollarSign: filledDollarSign,
    education: filledEducation,
    energy: filledEnergy,
    euro: filledEuro,
    filter: filledFilter,
    finance: filledFinance,
    gridView: filledGridView,
    growth: filledGrowth,
    headphone: filledHeadphone,
    heart: filledHeart,
    historicalData: filledHistoricalData,
    home: filledHome,
    honeycomb: filledHoneycomb,
    income: filledIncome,
    keys: filledKeys,
    language: filledLanguage,
    laptopAndMobile: filledLaptopAndMobile,
    layer: filledLayer,
    listView: filledListView,
    location: filledLocation,
    lock: filledLock,
    logistics: filledLogistics,
    logisticUp: filledLogisticUp,
    message: filledMessage,
    mic: filledMic,
    microscope: filledMicroscope,
    money: filledMoney,
    movie: filledMovie,
    pencil: filledPencil,
    people: filledPeople,
    people2: filledPeople2,
    person: filledPerson,
    phone: filledPhone,
    pipette: filledPipette,
    plane: filledPlane,
    posting: filledPosting,
    pound: filledPound,
    puzzle: filledPuzzle,
    reaction: filledReaction,
    repost: filledRepost,
    roundFlask: filledRoundFlask,
    settings: filledSettings,
    share: filledShare,
    speaker: filledSpeaker,
    testTube: filledTestTube,
    tools: filledTools,
    triangularFlask: filledTriangularFlask,
    truck: filledTruck,
    watch: filledWatch,
  },

  glyphs: {
    add,
    arrowCheck,
    arrowChevron,
    arrowDoubleLeft,
    arrowDoubleRight,
    arrowDown,
    arrowLeft,
    arrowRight,
    arrowTop,
    attention,
    bell,
    calendar,
    checkboxEmpty,
    checkboxFilled,
    close,
    dot,
    exportFile,
    favorite,
    filter: filterGlyph,
    mediaAudio,
    mediaFastForward,
    mediaPause,
    mediaPlay,
    mediaRewind,
    mediaStop,
    minus,
    options,
    placeholder,
    plus,
    progressBarCheckMark,
    progressBarComingUp,
    progressBarDisabled,
    progressBarIndicator,
    radioButtonFilled,
    radioButtonEmpty,
    radioButtonActive,
    radioButtonHover,
    search,
    settings: settingsGlyph,
    star,
    tooltipFilled,
    tooltipEmpty,
    upload,
    circleX,
    dotHalf,
    dots,
    goods,
    info,
    infoFilled,
    infoM,
    infoCircle,
    logo,
    radioButton,
    rental,
    service,
    starHalf,
    warning,
    facebook,
    slack,
    instagram,
    mail,
    snapchat,
    salesforce,
    twitter,
    skype,
    linkedin,
    teams,
    flickr,
    xing,
    warningM,
  },
}
